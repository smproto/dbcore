package dbcore

import (
	"fmt"
	"strings"
)

type Config struct {
	Host     string
	Port     int
	SslMode  string
	DbName   string
	User     string
	Password string
}

func (config *Config) CreateConnectionString() string {
	segments := make([]string, 0)

	if config.Host != "" {
		segments = append(segments, fmt.Sprintf("host=%s", config.Host))
	}
	if config.Port > 0 {
		segments = append(segments, fmt.Sprintf("port=%d", config.Port))
	}
	if config.DbName != "" {
		segments = append(segments, fmt.Sprintf("dbname=%s", config.DbName))
	}
	if config.User != "" {
		segments = append(segments, fmt.Sprintf("user=%s", config.User))
	}
	if config.Password != "" {
		segments = append(segments, fmt.Sprintf("password=%s", config.Password))
	}

	if config.SslMode != "" {
		segments = append(segments, fmt.Sprintf("sslmode=%s", config.SslMode))
	} else {
		segments = append(segments, "sslmode=disable")
	}

	return strings.Join(segments, " ")
}