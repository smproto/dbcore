package dbcore

import "github.com/jmoiron/sqlx"

type Database struct {
	*sqlx.DB
	Config *Config `apicore:"config" root:"db"`
}

func NewDatabase() *Database {
	return &Database{
	}
}

func (db *Database) Connect(driver string) error {
	sqlDb, err := sqlx.Open(driver, db.Config.CreateConnectionString())

	if err != nil {
		return err
	}

	db.DB = sqlDb
	return nil
}