package unittest

import (
	"bitbucket.org/smproto/apicore"
	"bitbucket.org/smproto/dbcore"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestConfigInjection_Success(t *testing.T) {
	context := apicore.NewContext()
	context.LoadMapConfig(map[string]interface{} {
		"db": map[string]interface{}{
			"host": "test",
		},
	})

	db := dbcore.NewDatabase()
	context.AddDependents(db)

	require.NoError(t, context.Install())
	require.Equal(t, "test", db.Config.Host)
}
