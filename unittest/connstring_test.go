package unittest

import (
	"bitbucket.org/smproto/dbcore"
	"fmt"
	"github.com/stretchr/testify/require"
	"strings"
	"testing"
)

func TestConnString_HostAssignment(t *testing.T) {
	config := dbcore.Config{
		Host: "test",
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, fmt.Sprintf("host=%s", config.Host)))
}

func TestConnString_PortAssignment(t *testing.T) {
	config := dbcore.Config{
		Port: 80,
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, fmt.Sprintf("port=%d", config.Port)))
}

func TestConnString_UserAssignment(t *testing.T) {
	config := dbcore.Config{
		User: "test",
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, fmt.Sprintf("user=%s", config.User)))
}

func TestConnString_PasswordAssignment(t *testing.T) {
	config := dbcore.Config{
		Password: "test",
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, fmt.Sprintf("password=%s", config.Password)))
}

func TestConnString_DbAssignment(t *testing.T) {
	config := dbcore.Config{
		DbName: "test",
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, fmt.Sprintf("dbname=%s", config.DbName)))
}

func TestConnString_SslEnabled(t *testing.T) {
	config := dbcore.Config{
		SslMode: "test",
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, fmt.Sprintf("sslmode=%s", config.SslMode)))
}

func TestConnString_SslDisabled(t *testing.T) {
	config := dbcore.Config{
	}

	connStr := config.CreateConnectionString()
	require.True(t, strings.Contains(connStr, "sslmode=disable"))
}


func TestConnString_SpaceDelimited(t *testing.T) {
	config := dbcore.Config{
		DbName: "test",
		User:   "testuser",
	}

	connStr := config.CreateConnectionString()
	segments := strings.Split(connStr, " ")

	require.True(t, len(segments) >= 2)
}
