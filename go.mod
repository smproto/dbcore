module bitbucket.org/smproto/dbcore

require (
	bitbucket.org/smproto/apicore v0.2.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/stretchr/testify v1.2.2
)
